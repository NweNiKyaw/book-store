-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2019 at 05:35 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `category_id` int(11) NOT NULL,
  `cover` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `summary`, `price`, `category_id`, `cover`, `created_date`, `modified_date`) VALUES
(3, 'PWD', 'alice', 'etu', 1.1, 3, 'aa.jpg', '2019-02-19 20:07:26', '2019-02-19 20:07:26'),
(4, 'U2U', 'alice', 'fairway', 3.3, 1, 'c.jpg', '2019-02-19 21:36:33', '2019-02-27 20:34:10'),
(6, 'Ubuntu Pocket Guide and Reference', 'Keir Thomas', 'General', 9.99, 2, 'd.jpg', '2019-02-20 21:52:10', '2019-02-21 20:46:11'),
(7, 'Exploring Programming Language Architecture in Prel', 'Bill Hails', 'Programming', 49.99, 6, 'b.jpg', '2019-02-20 21:53:27', '2019-02-21 20:45:57'),
(8, 'Introductin to Data Science', 'Leffrey Stanton', 'System Design', 4.99, 7, 'c.jpg', '2019-02-20 21:54:20', '2019-02-21 20:45:45'),
(16, 'Rock Star Developer', 'ei maung', 'software summary', 3.3, 1, '', '2019-02-28 20:14:45', '2019-02-28 22:38:09'),
(17, 'monalisa', 'smith', 'about actors', 1.1, 6, '', '2019-02-28 20:16:32', '2019-02-28 22:51:41'),
(18, 'National Automatic Irom', 'jame', 'Lampare de piloto', 2.1, 15, '', '2019-03-01 15:47:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remark` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `remark`, `created_date`, `modified_date`) VALUES
(1, 'Technology', 'Some remark', '2019-02-10 11:42:01', '2019-02-10 11:42:01'),
(2, 'Internet', 'mpt network', '2019-02-10 11:42:01', '2019-02-16 20:44:11'),
(3, 'Mobile', 'mobile remark', '2019-02-10 11:42:01', '2019-02-16 20:43:41'),
(6, 'Magazine', '', '2019-02-10 11:42:01', '2019-02-10 11:42:01'),
(7, 'Article', 'Another Remark', '2019-02-10 11:51:56', '0000-00-00 00:00:00'),
(9, 'Mobile', 'Mobile remark', '2019-02-16 11:10:22', '2019-02-16 11:10:22'),
(12, 'iphone', 'apple remark', '2019-02-16 20:45:09', '2019-02-16 20:45:09'),
(15, 'POP', 'dryer for hair', '2019-02-27 20:58:02', '2019-02-27 20:58:02'),
(16, 'OKI', 'Japanese Technology', '2019-02-27 21:14:39', '2019-02-27 22:23:19'),
(17, 'Ajax', '', '2019-03-23 11:16:41', '0000-00-00 00:00:00'),
(18, 'Ajax2', '', '2019-03-23 11:17:51', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `name`, `email`, `phone`, `address`, `status`, `created_date`, `modified_date`) VALUES
(1, '', '', '', '', 0, '2019-02-24 20:02:27', '2019-02-24 20:02:27'),
(2, '', '', '', '', 0, '2019-02-24 20:04:27', '2019-02-24 20:04:27'),
(3, 'n', 'zmt123@gmail.com', '12345678', 'litn epypew', 0, '2019-02-24 20:05:19', '2019-02-24 20:05:19'),
(4, '', '', '', '', 0, '2019-02-24 20:07:03', '2019-02-24 20:07:03'),
(5, '', '', '', '', 0, '2019-02-25 21:04:34', '2019-02-25 21:04:34'),
(6, '', '', '', '', 0, '2019-02-25 21:31:26', '2019-02-25 21:31:26'),
(7, 'nnk', 'nnk123@gmail.com', '09876543', 'taze', 0, '2019-03-01 00:23:46', '2019-03-01 00:23:46'),
(8, 'NweNiKyaw', 'nnk123@gmail.com', '097643', 'Hlaing township, shwehinthar road;', 0, '2019-03-01 16:32:55', '2019-03-01 16:32:55'),
(9, 'NweNiKyaw', 'nnk123@gmail.com', '097643', 'Hlaing township, shwehinthar road;', 0, '2019-03-01 16:33:27', '2019-03-01 16:33:27'),
(10, 'NweNiKyaw', 'nnk123@gmail.com', '0986543', 'ljhgfds', 0, '2019-03-02 09:47:31', '2019-03-02 09:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `orders_items`
--

CREATE TABLE `orders_items` (
  `id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_items`
--
ALTER TABLE `orders_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `orders_items`
--
ALTER TABLE `orders_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
