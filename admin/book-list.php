<?php 	
	include ("confs/config.php");
	include ("confs/auth.php");

	$result=mysqli_query ($conn, "SELECT books.*,categories.name FROM books LEFT JOIN categories ON books.category_id=categories.id ORDER BY books.created_date DESC");


 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>	Book List</title>
 	<link rel="stylesheet" href="css/style.css">
 </head>
 <body>
 <h1>Book List</h1>
 <ul class="menu">
 	<li><a href="book-list.php">Manage Books</a></li>
 	<li><a href="cat-list.php">Manage Categories</a></li>
 	<li><a href="orders.php">Manage Orders</a></li>
 	<li><a href="logout.php">Logout</a></li>
 </ul>

 <ul class="list">	
 	<?php 	while($row=mysqli_fetch_assoc($result)): ?>
 		<li>
 				<img src="covers/<?php 	echo $row['cover'] ?>" align="right" height="150">	
 				<b>	<?php 	echo $row['title'] ?></b>
 				<i>	by <?php 	echo $row['author'] ?></i>
 				<small>	( <?php 	echo $row['name'] ?>)</small>
 				<span>$ <?php 	echo $row ['price'] ?></span>
 				<div><?php 	echo $row['summary'] ?></div>
 				<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	</div>
 				[<a href="book-del.php?id=<?php echo $row['id']?>" class="del">Del</a>]
 				[<a href="book-edit.php?id=<?php echo $row['id']?>">Edit</a>]
 		</li>
 	<?php 	endwhile; ?>
 </ul>
 <a href="book-new.php" class="new">Book New</a>
 </body>
 </html>